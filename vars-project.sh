# ------ expects $project, $builddir to have been set
tutorbuilddir="$builddir"/tutor-"$project"
tutorvenvdir="$tutorbuilddir"/"$venv_tutor"
tutorvenvbindir="$tutorvenvdir"/bin
tutorprojectdir="$rootdir"/projects/"$project"
tutorallprojectsdir="$rootdir"/projects/__all__
tutorthemesdir="$tutorprojectdir"/themes
tutortemplatesprojectdir="$tutorprojectdir"/tutor/tutor/templates
tutortemplatesallprojectsdir="$tutorallprojectsdir"/tutor/tutor/templates
tutortemplatestutordir="$rootdir"/tutor/tutor/templates
tutortemplatesbuilddir="$tutorbuilddir"/tutor/templates
tutortemplateconfig_theirs="$tutortemplatesbuilddir"/config.yml
tutortemplateconfig_ours="$tutorprojectdir"/tutor/tutor/templates/config.yml
tutorrootthemedirparts=("$tutor_root" env build openedx themes)
tutorrootopenedxthemesdir=$(join-out / tutorrootthemedirparts)
tutor_plugins_root="$tutorprojectdir"/plugins
