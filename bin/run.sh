#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
. "$bindir"/functions.bash
. "$bindir"/local.bash
rootdir="$bindir"/..
. "$rootdir"/vars.sh
_ret=

tasks=(init build-images set-theme build-languages build-javascript update shell start stop)

project=
tutorbuilddir=

USAGE=$(printf "Usage: $0 {-p project} [opts] cmd...

where project is one of: %s
      cmd is one of: %s
      'update' is short-hand for all the steps from 'init -I --fh' to
        'build-languages', followed by 'start'.
      options for init:
        -I: don't destroy data or docker containers on init. Use this when
          rerunning init, for example if **/config.yml or settings/**/*.py changed,
          if the plugins changed, or to force a pull of our openedx platform
          commits during build-images. As long as you use -I and --fh
          this step is safe to rerun at any time, so it's also part of the
          'update' task.
        --fh: bypass error about tutor_root existing.
      options for set-theme:
        -t theme-name (required)
      options for build-images:
        --disable-docker-cache: force a full rebuild of the image.
      options for build-languages:
        --force-build-javascript-languages: always rebuild javascript
          languages. This is useful if the previous build crashed after
          checking if they needed rebuilding but before actually doing it.
" \
    "$(join-out ', ' projects)" \
    "$(join-out ', ' tasks)" \
)

opt_p=
opt_I=
opt_fh=
opt_t=
opt_disable_docker_cache=
force_build_javascript_languages=

while getopts hp:t:I-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        p) opt_p="$OPTARG" ;;
        t) opt_t="$OPTARG" ;;
        I) opt_I=yes ;;
        -)  OPTARG_KEY="${OPTARG%=*}"
            OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG_KEY in
                help)  warn "$USAGE"; exit 0 ;;
                fh) opt_fh=yes ;;
                disable-docker-cache) opt_disable_docker_cache=yes ;;
                force-build-javascript-languages) force_build_javascript_languages=yes ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

bash-with-venv () {
    local _src=$1
    local src="source $bindir/functions.bash; cmd-source $venvdir/bin/activate; $_src"
    cmd bash -c "$src"
}

bash-with-venv-tutor () {
    local _src=$1
    # local src="source $bindir/functions.bash; cmd-source $tutorbuilddir/$venv_tutor/bin/activate; $_src"
    local src="source $bindir/functions.bash; cmd-source $tutorvenvbindir/activate; $_src"
    cmd bash -c "$src"
}

bash-with-venv-tutor-quiet () {
    local _src=$1
    local src="source $tutorvenvbindir/activate; $_src"
    bash -c "$src"
}

init-tutor-pip-install () {
    local src="cmd pip install -e ."
    fun bash-with-venv-tutor "$src"
}

init-tutor-config () {
    info "patching $tutortemplateconfig_theirs"
    fun yaml-merge-files "$tutortemplateconfig_theirs" "$tutortemplateconfig_theirs" "$tutortemplateconfig_ours"
    fun yaml-reorder "$tutortemplateconfig_theirs" "${config_keys_move_to_begin[@]}"

    # xport-secret AC_SMARTHOST_PASSWORD "$email_password"
    # xport AC_SMARTHOST_ADDRESS_CANONICAL "$email_host"
    # xport AC_SMARTHOST_PORT "$email_port"
    # xport AC_SMARTHOST_USER "$email_address"

    local conffile
    local stub
    conffile="$tutortemplatesbuilddir"/local/docker-compose.yml
    info "patching $conffile"
    cmd python3 "$scriptsdir"/init-docker-compose.py "$conffile"

    local platform
    for platform in lms cms; do
        stub=apps/openedx/settings/"$platform"/production.py
        local stubappend=apps/openedx/settings/"$platform"/production.append.py
        conffile="$tutortemplatesbuilddir"/"$stub"
        info "patching settings file $conffile (platform = $platform)"
    done

    mci
    mcb fun redirect-out "$conffile" cat
    mcb   "$tutortemplatestutordir"/"$stub"
    mcb   "$tutortemplatesprojectdir"/"$stubappend"
    mcg

    info 'adding AC_RANDOM_STRING to config.yml'
    local date
    date=$(date +%s)
    cmd redirect-out-append "$tutortemplateconfig_theirs" echo "AC_RANDOM_STRING: $date"

    info 'setting SMTP variables in config.yml'
    sed -i 's,SMTP_HOST:.\+,SMTP_HOST: '"$email_host", "$tutortemplateconfig_theirs"
    sed -i 's,SMTP_PORT:.\+,SMTP_PORT: '"$email_port", "$tutortemplateconfig_theirs"
    sed -i 's,SMTP_USERNAME:.\+,SMTP_USERNAME: '"$email_username", "$tutortemplateconfig_theirs"
    sed -i 's,SMTP_PASSWORD:.\+,SMTP_PASSWORD: '\'"$email_password"\', "$tutortemplateconfig_theirs"

    local plugin
    local src
    for plugin in "${plugins[@]}"; do
        src="tutor plugins enable ""$(shell-quote "$plugin")"
        fun bash-with-venv-tutor "$src"
    done
}

init-tutor-config-language-code () {
    local stublms_theirs=apps/openedx/settings/lms/production.py
    local stubcms_theirs=apps/openedx/settings/cms/production.py
    local stubappend=apps/openedx/settings/common/production.language-code.append.py
    local srcfile="$tutortemplatesprojectdir"/"$stubappend"
    local conffilelms_theirs="$tutortemplatesbuilddir"/"$stublms_theirs"
    local conffilecms_theirs="$tutortemplatesbuilddir"/"$stubcms_theirs"

    info "patching lms settings file with language code ($conffilelms_theirs)"
    fun redirect-out-append "$conffilelms_theirs" cat "$srcfile"

    info "patching cms settings file with language code ($conffilecms_theirs)"
    fun redirect-out-append "$conffilecms_theirs" cat "$srcfile"
}

kill-stale-containers () {
    local x
    x=$(docker ps -aq --filter=name=^"$project_name")
    if [ -z "$x" ]; then return 0; fi
    cmd docker rm -f $x
}

init-tutor () {
    # local src="cmd tutor config save; cmd tutor local init"
    # fun bash-with-venv-tutor "$src"
    local src
    src='cmd tutor config save'
    fun bash-with-venv-tutor "$src"
    src='cmd tutor local init'
    fun bash-with-venv-tutor "$src"
}

# --- @temp
fix-dockerfile-11-1-1 () {
  local file="$tutor_root"/env/build/openedx/Dockerfile
  local filetmp="$tutor_root"/env/build/openedx/Dockerfile-tmp

  read -rd END prog <<'EOT'
import re
import sys
with open(sys.argv[1]) as fh:
    lines=fh.readlines()
lines.insert(98, '''RUN pip install git+https://github.com/py2neo-org/py2neo@py2neo-3.1.2\n''')
lines.insert(88, '''RUN sed -i '/py2neo/d' ./requirements/edx/base.txt\n''')
lines = [re.sub(r'curl https', 'curl -L https', l) for l in lines]
# comment out the line:
#   RUN curl https://github.com/overhangio/edx-platform/commit/81b18b1b97ca89e2d941c9f7283e3f6e385c5f92.patch | git apply -
lines = [re.sub(r'^(.+81b18b1b97ca89e2d941c9f7283e3f6e385c5f92.+)$', r'# \1', l) for l in lines]
print(''.join(lines))
END
EOT

  cmd redirect-out "$filetmp" python3 -c "$prog" "$file"
  cmd mv -f "$filetmp" "$file"
}

tutor-render-templates () {
    local srcdir
    local dstdir
    local src
    local i
    for i in ''; do
        srcdir="$tutorbuilddir"/tutor/templates/"$i"
        dstdir="$tutor_root"/env/"$i"
        src="cmd tutor config render $srcdir $dstdir"
        fun bash-with-venv-tutor "$src"
    done
}

tutor-build-images () {
    local disable_docker_cache=$1
    local img
    local src
    local arg
    local srcparts=()
    # --- this replaces the values at the top of the Dockerfile.
    # --- there is also a step (see the openedx-patch-via-git) plugin, which
    # replaces the values at the bottom.
    # --- normally it's enough to only change the ones at the bottom, so the
    # build goes much faster: build the image, then apply our custom patches to
    # edx-platform.
    local buildargs=(
        EDX_PLATFORM_REPOSITORY="$edx_platform_repository"
        EDX_PLATFORM_VERSION="$edx_platform_version"
    )
    local optsary=()
    if yes disable_docker_cache; then optsary+=(--no-cache); fi
    local opts=$(join-out ' ' optsary)
    for img in "${images[@]}"; do
        srcparts+=(cmd tutor images build "$opts")
        srcparts+=("$(shell-quote "$img")")
        for arg in "${buildargs[@]}"; do
            srcparts+=(--build-arg "$(shell-quote "$arg")")
        done
        src=$(join-out ' ' srcparts)
        info "$src"
        fun bash-with-venv-tutor "$src"
    done
}

diff-theme () {
    local theme=$1
    local left="$tutorthemesdir"/"$theme"/theme
    local right="$tutorrootopenedxthemesdir"/"$theme"
    if [ ! -d "$right" ]; then return 1; fi
    cmd diff -Zr "$right" "$left"
}

check-themes-dirty () {
    # --- @todo
    local themes=(januari)

    local theme
    for theme in "${themes[@]}"; do
        if ! fun diff-theme "$theme"; then
            return 0
        fi
    done
    return 1
}

remove-tutor-root-themes-dir () {
    if ! fun check-themes-dirty; then
        info 'no changes to themes, Docker cache will be used'
        return 0
    fi
    info 'themes need updating, removing and rebuilding theme dir in TUTOR_ROOT if it exists'
    fun safe-rm-dir-array tutorrootthemedirparts
}

init () {
    local reinit=$1
    local force_home_exists=$2
    if [ -d "$tutor_root" ]; then
        if yes force_home_exists; then
            warn $(printf "directory %s exists, ignoring." "$(cyan "$tutor_root")")
        else
            error $(printf "directory %s exists, aborting (use --fh to ignore)." "$(bright-red "$tutor_root")")
        fi
    fi

    if ! yes reinit; then
        fun safe-rm-dir-array builddirparts
        cwd "$venvdirbase" cmd python3 -m venv "$venv"
        local src="cmd pip install -r $rootdir/requirements.txt"
        fun bash-with-venv "$src"
        mkd "$builddir"
        cpa "$tutordir" "$tutorbuilddir"
        cwd "$tutorbuilddir" cmd python3 -m venv "$venv_tutor"
        cwd "$tutorbuilddir" fun init-tutor-pip-install
        fun kill-stale-containers
    fi
    fun init-tutor-config
    if ! yes reinit; then
        fun init-tutor
    fi
    fun init-tutor-config-language-code
    fun tutor-render-templates

# --- @temp
fun fix-dockerfile-11-1-1

    # --- ensure empty dir so that diffs work; for now it's the only file.
    cmd rm -f "$tutortemplatesbuilddir"/build/openedx/themes/README
}

process-theme () {
    local themedirbase=$1
    local themedirfull=$2
    local src="tutor config render --extra-config \"$themedirfull\"/config.yml \"$themedirfull\"/theme \"$tutorrootopenedxthemesdir\"/\"$themedirbase\""
    fun bash-with-venv-tutor "$src"
}

build-images () {
    local themedir
    local foundone=
    local disable_docker_cache=$1
    fun remove-tutor-root-themes-dir
    while read -r -d $'\0' themedir; do
        foundone=yes
        fun process-theme "$themedir" "$tutorthemesdir"/"$themedir"
    done < <( cd "$tutorthemesdir"; find 2>/dev/null . -maxdepth 1 -mindepth 1 -type d -print0 )
    if [ -z "$foundone" ]; then
        info "No themes found, ok."
    fi
    cwd "$tutorbuilddir" fun tutor-build-images "$disable_docker_cache"
    fun _stop
}

shell () {
    local f="$tmp"/edx-build.rc
    cmd redirect-out "$f" echo ''
    cmd redirect-out-append "$f" cat "$tutorvenvbindir"/activate
    cmd redirect-out-append "$f" echo "export TUTOR_ROOT=\"$tutor_root\""
    cmd redirect-out-append "$f" echo "export TUTOR_PLUGINS_ROOT=\"$tutor_plugins_root\""
    cmd bash --rcfile "$f"
}

begin () {
    project=$1

    if not_contains projects "$project"; then
        error $(printf "Invalid project %s" "$(bright-red "$project")")
    fi
    cmd-source "$rootdir"/vars-local-"$project".sh
    cmd-source "$rootdir"/vars-project.sh
}

# --- will start lms and cms if necessary, then shut them down.
set-theme () {
    local theme=$1
    if [ -z "$theme" ]; then
        error "$USAGE"
    fi
    local lms_host=$(bash-with-venv-tutor-quiet "tutor config printvalue LMS_HOST")
    local cms_host=$(bash-with-venv-tutor-quiet "tutor config printvalue CMS_HOST")
    local src
    src="tutor local start -d"
    fun bash-with-venv-tutor "$src"
    read -r -d 💣 src <<- EOT
		cmd () { echo "* \$@"; "\$@"; }
		cd /openedx/staticfiles
		if [ -e "__theme__" -a ! -L "__theme__" ]; then
			echo "__theme__" exists and is not a symlink, exiting.
			exit 1
		fi
		cmd rm -f __theme__
		cmd ln -s "$theme" __theme__
		💣
	EOT
    info "lms: linking /openedx/staticfiles/__theme__ -> /openedx/staticfiles/$theme"
    cmd docker exec tutor_lms_1 bash -c "$src"
    src="tutor local settheme \"$theme\" \"$lms_host\" \"$cms_host\""
    fun bash-with-venv-tutor "$src"
    fun _stop
}

_augment-languages () {
    local i
    local j
    info 'augment-languages: start lms & cms'
    src="tutor local start lms -d; tutor local start cms -d"
    for i in lms cms lms-worker cms-worker; do
        for j in "$localesextradir"/*; do
            if [[ "$j" =~ \.ya?ml$ ]]; then continue; fi
            cmd docker cp "$j" "$project_name"_"$i"_1:/openedx/edx-platform/conf/locale
        done
    done
}

build-languages () {
    local force_build_javascript_languages=$1
    local src="tutor local start lms -d; tutor local start cms -d"
    fun bash-with-venv-tutor "$src"
    local settingsdir="$tutor_root"/env/build/openedx/settings
    local variant
    local py
    for variant in lms cms; do
        for py in i18n.py assets.py; do
            cpa "$settingsdir"/"$variant"/"$py" "$tutor_root"/env/apps/openedx/settings/"$variant"
        done
    done
    local comm=("$bindir"/compile-languages.py -j "$localesextradir")
    cmd-print "${comm[@]}"
    local needs_rebuild_js
    if yes force_build_javascript_languages; then
        needs_rebuild_js=yes
    else
        needs_rebuild_js=$(${comm[@]})
        if [ ! "$needs_rebuild_js" = yes -a ! "$needs_rebuild_js" = no ]; then
            error "Expecting yes or no from compile-languages.py, got $needs_rebuild_js"
        fi
    fi
    info $(printf 'rebuild js translations: %s' "$(yellow "$needs_rebuild_js")")
    cmd "$bindir"/compile-languages.py "$tutor_root" "$docker_image_openedx" "$localesextradir"
    fun _augment-languages

    local src
    if yes needs_rebuild_js; then
        for variant in lms cms; do
            src='./manage.py '"$variant"' --settings=tutor.i18n compilejsi18n'
            fun bash-with-venv-tutor 'tutor local exec '"$variant $src"
            src='openedx-assets collect --settings=tutor.assets --systems='"$variant"
            fun bash-with-venv-tutor 'tutor local exec '"$variant $src"
        done
    fi
    # --- necessary to force the languages to refresh
    info 'build-languages: stop lms* & cms*'
    fun _stop
}

_stop () {
    local src='tutor local stop lms; tutor local stop cms; tutor local stop lms-worker; tutor local stop cms-worker'
    fun bash-with-venv-tutor "$src"
}

# --- @wip
# --- this is for when javascript files change in edx-platform but we don't want
# to build the whole docker image again.
# --- it's a necessary step but maybe not the only one.

build-javascript () {
    local settingsdir="$tutor_root"/env/build/openedx/settings
    local variant
    local src
    src="tutor local start lms -d; tutor local start cms -d"
    fun bash-with-venv-tutor "$src"
    # --- @todo repeated in build-languages
    for variant in lms cms; do
        cpa "$settingsdir"/"$variant"/assets.py "$tutor_root"/env/apps/openedx/settings/"$variant"
    done
    for variant in lms cms; do
        src='tutor local exec '"$variant"' openedx-assets collect --settings=tutor.assets --systems='"$variant"
        fun bash-with-venv-tutor "$src"
    done
}

start () {
    src="tutor local start -d"
    fun bash-with-venv-tutor "$src"
}

stop () {
    src="tutor local stop"
    fun bash-with-venv-tutor "$src"
}

beep () {
    echo -n ""
}

go () {
    local project=$opt_p
    local task
    for task in "$@"; do
        if not_contains tasks "$task"; then
            error $(printf "Invalid task %s" "$(bright-red "$task")")
        fi
    done
    for task in "$@"; do
        fun begin "$project"
        xport TUTOR_ROOT "$tutor_root"
        xport TUTOR_PLUGINS_ROOT "$tutor_plugins_root"
        if [ "$task" != init -a "$task" != update ]; then
          cmd-source "$venvdir"/bin/activate
        fi
        if [ "$task" = init ]; then fun init "$opt_I" "$opt_fh"
        elif [ "$task" = build-images ]; then fun build-images "$opt_disable_docker_cache"
        elif [ "$task" = set-theme ]; then fun set-theme "$opt_t"
        elif [ "$task" = build-languages ]; then fun build-languages "$force_build_javascript_languages"
        elif [ "$task" = shell ]; then fun shell
        elif [ "$task" = start ]; then fun start "$opt_t"
        elif [ "$task" = stop ]; then fun stop "$opt_t"
        elif [ "$task" = build-javascript ]; then fun build-javascript
        elif [ "$task" = update ]; then
            fun init yes yes
            cmd-source "$venvdir"/bin/activate
            fun build-images "$opt_disable_docker_cache"
            fun set-theme "$opt_t"
            fun build-languages "$force_build_javascript_languages"
            fun start "$opt_t"
        fi
    done
    fun beep
    info 'All done.'
}

if [ "$#" = 0 -o -z "$opt_p" ]; then
    error "$USAGE"
fi

fun go "$@"
