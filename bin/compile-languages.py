#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import python_paths # noqa

from functools import partial
from glob import glob
import os
import os.path
from sys import argv, stdout

from acatpy.io.io import cmd, die, info
from acatpy.io.speak import bright_red

USAGE = """Usage:

# --- checks whether js translations need compiling and prints 'yes' or 'no' on stdout
%s -j { locales-dir }

  or

# --- merges some .po files, merges and compiles all .po files to .mo files
%s { tutor-root } { docker-image-name } { locales-dir }

""" % (argv[0], argv[0])

TUTOR_PREFIX = 'tutor'

# --- .mo files don't correspond one-to-one to .po files, so use a simple
# heuristic: update everything if any .po file in a directory is newer than
# the oldest .mo file in that directory.

def dir_is_dirty(the_dir, moglob, poglob):
    oldest_mo = None
    for mo in glob('%s/LC_MESSAGES/%s' % (the_dir, moglob)):
        this_mo = os.stat(mo).st_mtime
        if oldest_mo is None:
            oldest_mo = this_mo
        else:
            oldest_mo = min(this_mo, oldest_mo)
    for po in glob('%s/LC_MESSAGES/%s' % (the_dir, poglob)):
        this_po = os.stat(po).st_mtime
        if this_po > oldest_mo:
            return True
    return False

def _check_needs_build_xxx(locales_dir, globs):
    found_one = False
    for langdir in glob('%s/*' % locales_dir):
        found_one = True
        for moglob, poglob in globs:
            if dir_is_dirty(langdir, moglob, poglob):
                return True
    if not found_one:
        die("Invalid locales-dir: %s" % bright_red(locales_dir))
    return False

def check_needs_build_normal(locales_dir):
    return _check_needs_build_xxx(locales_dir, [
        ('*.mo', '*.po'),
    ])

# --- @todo build could be sped up by checking which variant needs updating.
def check_needs_build_js(locales_dir):
    return _check_needs_build_xxx(locales_dir, [
        ('djangojs-*.mo', 'djangojs-*.po'),
        ('underscore.mo', 'underscore.po'),
    ])

def rebuild_mo_files(tutor_root, docker_image_name, locales_dir):
    mounts = [
        (
            os.path.abspath(locales_dir),
            '/openedx/edx-platform/conf/locale/'
        ),
        (
            '%s/env/apps/openedx/settings/lms/' % tutor_root,
            '/openedx/edx-platform/lms/envs/tutor'
        ),
        (
            '%s/env/apps/openedx/config/' % tutor_root,
            '/openedx/config/'
         ),
    ]
    mount = [x for m in mounts for x in ['-v', ':'.join(m)]]
    comm = [
        'bash', '-c',
        'cd conf/locale && i18n_tool generate -v --config=./config-extra.yaml',
    ]
    cmd(
        ['docker', 'run', '--rm', '-it', *mount, docker_image_name, *comm],
        verbose=True, die=True,
    )

def go(tutor_root, docker_image_name, locales_dir):
    needs_build_normal = check_needs_build_normal(locales_dir)
    needs_build_js = check_needs_build_js(locales_dir)

    if not needs_build_normal:
        info('.mo files are up to date')
        return

    info('rebuilding .mo files')
    rebuild_mo_files(tutor_root, docker_image_name, locales_dir)

    if not needs_build_js:
        info('javascript translations are up to date')
        return

# eh ... optparse
def process_args():
    if len(argv) < 2:
        return None
    if argv[1] == '-j':
        if len(argv) != 3:
            return None
        locales_dir = argv[2]
        def f():
            needs_build_js = check_needs_build_js(locales_dir)
            stdout.write('yes' if needs_build_js else 'no')
            stdout.flush()
        return f
    if len(argv) == 4:
        tutor_root, docker_image_name, locales_dir = argv[1:]
        return partial(go, tutor_root, docker_image_name, locales_dir)
    return None

f = process_args()

if not f:
    die(USAGE)

f()
