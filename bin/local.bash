contains () {
    local aryname=$1
    local x=$2
    local i=0
    local j
    local len
    eval "len=\${#${aryname}[@]}"
    while ((i < len)); do
        eval "j=\${$aryname[$i]}"
        if [ "$j" = "$x" ]; then
            return 0
        fi
        let i=i+1
    done
    return 1
}

not_contains () {
    if contains "$@"; then return 1
    else return 0
    fi
}

# --- yaml-merge-files <outfile> <infile...>
# --- infiles will be merged from left to right.

yaml-merge-files () {
    local py=$(cat <<EOT
import yaml
from sys import argv
result = {}
argv.pop(0)
outfile = argv.pop(0)
for file in argv:
    with open(file, 'r') as fh:
        this = yaml.load(fh, Loader=yaml.Loader)
        result = {**result, **this}
with open(outfile, 'w') as fh:
    fh.write(yaml.dump(result))
EOT
    )
    cmd-print python3 -c '[inline source]' "$@"
    python3 -c "$py" "$@"
}

# --- yaml-reorder <yamlfile> <key>....
# will move the given keys to the beginning.
yaml-reorder () {
    local py=$(cat <<EOT
import yaml
from sys import argv
argv.pop(0)
yamlfile = argv.pop(0)
with open(yamlfile, 'r') as fhr:
    yam = yaml.load(fhr, Loader=yaml.Loader)
with open(yamlfile, 'w') as fhw:
    pass
with open(yamlfile, 'a') as fhw:
    for key in argv:
        val = yam.pop(key)
        this = { key: val, }
        fhw.write(yaml.dump(this, default_flow_style=False))
    fhw.write(yaml.dump(yam))
EOT
    )
    cmd-print python3 -c '[inline source]' "$@"
    python3 -c "$py" "$@"
}

xport-secret () {
    local var="$1"
    local val="${2:-}"
    local localvar

    sayf "$(printf "[ %s ] [ %s ] %s ***" "$(yellow env)" "$(cyan secret)" "$(bright-red "$var")" )"
    read -d '' localvar <<< "$val" || true
    export "$var"="$localvar"
}

yes () {
    local varname=$1
    local val
    eval "val=\${$varname:-}"
    if [ "$val" = yes ]; then
        return 0
    fi
    return 1
}

md5sum-dir () {
    local retvar=$1
    local path=$2
    local sum
    sum=$(find -L "$path" -type f -exec cat {} \; | md5sum | awk '{print $1}')
    retvar "$retvar" "$sum"
}
