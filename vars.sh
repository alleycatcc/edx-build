tutordir="$rootdir"/tutor
builddirparts=("$rootdir" build)
builddir=$(join-out / builddirparts)
scriptsdir="$rootdir"/scripts
localesextradir="$rootdir"/locales-extra
venvdirbase="$rootdir"
venv=venv
venvdir="$venvdirbase/$venv"

edx_platform_repository=https://gitlab.com/alleycatcc/edx-platform
edx_platform_version=alleycat/koa.1.6

projects=(woon)
venv_tutor=venv

# --- custom images which need building.
images=(openedx)
plugins=(
     # --- @todo this is currently a woon-specific plugin
     # --- @todo this plugin hardcodes the edx_platform_version; find better
     # way?
    openedx-patch-via-git
    install-vim
)

config_keys_move_to_begin=(
    DOCKER_REGISTRY
    LMS_HOST
    OPENEDX_MYSQL_DATABASE
)
