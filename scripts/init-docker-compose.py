#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
from sys import argv

INDENT = 2

DO_FIX_SMTP = False

src = argv[1]

def indent(n, xs):
    return ['%s%s' % (' ' * n, x) for x in xs]

_smtp_stanza = '''
smtp:
  image: {{ DOCKER_IMAGE_SMTP }}
  restart: unless-stopped
  depends_on:
    - lms-worker
    - cms-worker
  environment:
    AC_SMARTHOST_ADDRESS_CANONICAL: %s
    AC_SMARTHOST_PORT: %s
    AC_SMARTHOST_USER: %s
    AC_SMARTHOST_PASSWORD: %s
    AC_RELAY_NETS: lms,cms,lms-worker,cms-worker
'''

def mk_smtp_stanza():
    ac_pass = os.environ['AC_SMARTHOST_PASSWORD']
    ac_addr = os.environ['AC_SMARTHOST_ADDRESS_CANONICAL']
    ac_port = os.environ['AC_SMARTHOST_PORT']
    ac_user = os.environ['AC_SMARTHOST_USER']
    return _smtp_stanza % (ac_addr, ac_port, ac_user, ac_pass)

def fix_smtp(contents):
    results = []
    smtp_stanza = mk_smtp_stanza()
    # --- this assumes that '{% if RUN_SMTP %}' occurs exactly once on a line all by itself.
    # (the string occurs several times, but only once on its own line)
    in_stanza = False
    for line in contents.split('\n'):
        if in_stanza:
            if re.search(r'% endif %', line):
                in_stanza = False
                new = smtp_stanza.split('\n')
                new = indent(2, new)
                results.extend(new)
            else:
                pass
            continue
        if re.search(r'^\s*\{% if RUN_SMTP %\}\s*$', line):
            in_stanza = True
            continue
        results.append(line)
    return results

with open(src, 'r') as fh:
    contents = fh.read()

if DO_FIX_SMTP:
    results = fix_smtp(contents)
    with open(src, 'w') as fh:
        fh.write('\n'.join(results))
