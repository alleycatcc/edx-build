# --- must match PROJECT_NAME in config.yml
project_name=tutor
# --- must match DOCKER_IMAGE_OPENEDX in config.yml
docker_image_openedx=openedx-woon-local:latest

tmp=/tmp

tutor_root=/opt/alleycat/tutor-home/woon

# --- local mail
email_host=172.17.0.1
email_port=587
email_username=xxx
email_password=xxx

# --- mail via smarthost
# email_host=smtp.provider.com
# email_address=openedx-woon@alleycat.cc
# email_password=jasdfjasdufqjwefjasdufxzjvznxcvzau
# email_port=587
