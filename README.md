## General

Be sure to install `pydash` on your system (needed for `alleycat-py`).

Create config files based on 'example.sh' files.

Usage: bin/build.sh -h

## Useful commands in the container

./manage.py lms --settings tutor.production createsuperuser --username <username>
./manage.py cms --settings tutor.production dump_course_ids
./manage.py cms --settings tutor.production delete_course <id>

## Info about our build process

### Intro

Why do we need a custom build?

- tutor packs everything into the command `tutor local quickstart`, which is
  really handy, but also a bit too magical: it performs a lot of steps and
  it's not clear what it's doing.

- we want to be able to still use tutor, but:
    - patch the docker-compose files before we start. why do we do this?
        - to avoid building images we don't need (e.g. elasticsearch, smtp).
        - to edit URLs before the build starts (e.g. to use our custom
          upstream for edx-platform)
            - why do we have a custom upstream for this? in order to
              implement features and improvements without having to make
              pull-requests and get them accepted.
                - actually it would be far preferable if we did get the
                  changes accepted into upstream, but this takes effort
                  and is not always practical given time & deadline
                  constraints.
    - hook into the build process starting at an intermediate step -- for
      example, after changing some translations or theme stuff.
        - we are not uploading our translations to edifex, though we could
          in the future. currently we work on them locally and patch them
          into the code.
       - working on the translations is quite a pain, because while some of
         them use simple gettext .mo/.po files, others get baked into the
         .js files, which requires restarting the build at a much earlier
         step.

### Document our build steps

- ?
- ?
- ?

### Migration strategy

- perform a full build using the current tutur version on a dev machine (a
  droplet would be best).
- scp the entire openedx home directory (e.g. /opt/alleycat/tutur-home) from
  the production machine to the dev machine.
- restart -- the dev machine should now have an exact copy of the running
  system
- now upgrade tutor using pip.
- find the upstream edx-platform commit that this version of tutor is based
  on.
- rebase our edx-platform against this commit and push to
  alleycatcc/edx-platform. hopefully this won't be too painful ... of course
  it would be better to get our changes accepted to upstream and not do
  this.
- restart the system and see if the migrations happen automatically --
  check!
- if so, ...
