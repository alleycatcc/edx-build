��          D      l       �      �   #   �      �   �   �   �  �     r  &   �     �    �                          Account Information Help Translate into {beta_language} Order History You have set your language to {beta_language}, which is currently not fully translated. You can help us translate this language fully by joining the Transifex community and adding translations from English for learners that speak {beta_language}. Project-Id-Version: 0.1a
Report-Msgid-Bugs-To: openedx-translation@googlegroups.com
PO-Revision-Date: 2019-02-26 13:12+0000
Last-Translator: Quentin Schoemaker <quentin@quentins.nl>, 2020
Language-Team: Dutch (Netherlands) (https://www.transifex.com/open-edx/teams/6205/nl_NL/)
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Accountinformatie Help ons vertalen naar {beta_language} Bestelhistoriek Je hebt je taal ingesteld op {beta_language}, die momenteel niet volledig is vertaald. Je kunt ons helpen deze taal volledig te vertalen door lid te worden van de Transifex-gemeenschap en vertalingen uit het Engels toe te voegen voor studenten die {beta_language} spreken. 