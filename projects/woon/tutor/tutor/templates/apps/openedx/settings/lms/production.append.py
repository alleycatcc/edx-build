PLATFORM_TWITTER_ACCOUNT = '@stichtingwoon'
PLATFORM_FACEBOOK_ACCOUNT = 'https://nl-nl.facebook.com/wooninfo'

AC_PLATFORM_LINKEDIN_ACCOUNT = 'https://nl.linkedin.com/company/wooninfo'

WIKI_ENABLED = False

FEATURES = {
    **FEATURES,
    # --- true: /courses uses search; false: /courses uses browse.
    'ENABLE_COURSE_DISCOVERY': False,
    # --- search box on the dashboard page.
    'ENABLE_DASHBOARD_SEARCH': False,
    'ENABLE_DISCUSSION_SERVICE': False,
    'PREVENT_CONCURRENT_LOGINS': False,
}

AC_REGISTRATION_FOOTER_LINK = 'privacy'
AC_PRIVACY_POLICY = 'https://wooninfo.nl/organisatie/privacy-statement'
ACTIVATION_EMAIL_SUPPORT_LINK = 'https://www.wooninfo.nl/contact'
