# --- We want the default language to be NL, and both 'nl_NL' and 'nl-nl'
# work fine as far as the webapp is concerned. But manage.py wants either
# 'nl-nl' or nothing ('nl_NL' causes it to fail). We use 'nl-nl' here, which
# works all around.
LANGUAGE_CODE = 'nl-nl'
