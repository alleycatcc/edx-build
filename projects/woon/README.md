To enable maintenance mode on wooninfo server:

Change apache config, in the https section, to look like:

    #ProxyPass / http://localhost:<port>/
    #ProxyPassReverse / http://localhost:<port>/
    RewriteRule ^/.+ https://%{HTTP_HOST}/ [L,R=301]

Then

    sudo httpd -k graceful
